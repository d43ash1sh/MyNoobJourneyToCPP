#include <iostream>

using namespace std;
int main(){
  cout << "Control Structure in C++" <<endl ;
  cout << "SelectionControlStructure - If else-if else ladder" <<endl ;

   int age;
     cout << "Tell me your age !" <<endl ;
     cin >> age;
//     if(age<18){
//         cout << "You can't visit my site right now" <<endl;
//     }
//     else if(age==18){
//         cout << "You are a kid ! Your are get a kid pass to my site" <<endl;
//     }
//     else{
//         cout << "Welcome to my site !" << endl;
//     }

//cout << "SelectionControlStructure - SwitchCase Statements" <<endl ;
switch (age) {
    case 18:
    cout << "You are 18!"<< endl;
    break;
    case 22:
    cout << "You are 22!"<< endl;
    break;
    case 2:
    cout << "You are 2!";
    break;
    
    default:
    cout << "No valid cases !" << endl;
    break;
}
cout << "Done with SwitchCase";
    return 0;
}
