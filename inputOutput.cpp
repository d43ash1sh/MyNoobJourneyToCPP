#include <iostream>; // <iostream> input and output stream

/* stream= a flow of data into or out of a program.
output stream = the direction of flow of byte takes place from main memory to the output device(Display)
input stream = the direction of flow of byte takes place from input device (Keyboard) to the main memory.
*/
using namespace std; /* standard namespace */
int main(){
    int num1, num2;
    cout<< "Enter the value of number 1:\n"; // << 'insertion operator in c++'
    cin>> num1; // 'Extraction operator in c++'
     cout<< "Enter the value of number 2:\n";
    cin>> num2;
    cout<< "The value of num1 and num2 :"<< num1+num2;
    return 0;
}
