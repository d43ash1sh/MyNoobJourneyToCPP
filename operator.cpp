/* There are two types of headerFiles 
A. System HeaderFile (it's comes with the compiler */
#include <iostream>; 
/*
B. User defined HeaderFile (it's written by the programmer
#include "this.h" ==> must be present in the current directory otherwise it create an  error 

<iostream> input and output stream
stream = a flow of data into or out of a program.
output stream = the direction of flow of byte takes place from main memory to the output device(Display)
input stream = the direction of flow of byte takes place from input device (Keyboard) to the main memory.
*/
using namespace std;
int main(){
    int a=4, b= 5; 

    cout<< "Following are the types of Operators in C++"<<endl;
    cout<<endl;
    cout<< "Arithmetic Operators"<<endl;
    cout<< "Give the value of a = 4 and b = 5,"<<endl;
    cout<< "The value of (a + b) is: "<<a+b<<endl;
    cout<< "The value of (a - b) is: "<<a-b<<endl;
    cout<< "The value of (a x b) is: "<<a*b<<endl;
    
    cout<< "The value of (a / b) is: "<<a/b<<endl;
    cout<< "The value of (a % b) is: "<<a%b<<endl;
   // at this point the value of a is = 4 
    cout<< "The value of (a++) is: "<<a++<<endl;
    cout<< "The value of (a--) is: "<<a--<<endl;
    cout<< "The value of (++a) is: "<<++a<<endl;
    
   //cout<< "Assignment Operators"<<endl;
    /* Assignment Operators
    int a = 4, b = 5;
    char d = 'd' ;
     */
     
     // Comparsion Operators
    cout<<endl;
    cout<< "Comparsion Operators"<<endl; 
    cout<<"The value of a == b is "<<(a==b)<<endl;
    cout<<"The value of a != b is "<<(a!=b)<<endl;
    cout<<"The value of a >= b is "<<(a>=b)<<endl;
    cout<<"The value of a <= b is "<<(a<=b)<<endl;
    cout<<"The value of a > b is "<<(a>b)<<endl;
    cout<<"The value of a < b is "<<(a<b)<<endl;
    cout<<endl;
    
    // Logical operators
    cout<<"Logical Operators"<<endl;
    cout<<"The value of this logical and operator ((a==b) && (a<b)) is:"<<((a==b) && (a<b))<<endl; 
    cout<<"The value of this logical or operator ((a==b) || (a<b)) is:"<<((a==b) || (a<b))<<endl; 
    cout<<"The value of this logical not operator (!(a==b)) is:"<<(!(a==b))<<endl; 

    return 0;
}
