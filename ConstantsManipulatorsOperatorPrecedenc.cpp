#include <iostream>
#include <iomanip>
using namespace std;

int main() {
    
    cout << "welcome to my repository | MyNoobJourneyToC++ "<<endl;
    cout << "Constants, Manipulators & Operator Precedence"<<endl;
   
    //int a = 34;
    //cout << " The value of a was : "<< a << endl;
   // char c = 'c';
   // cout << "Given the value of (int a) = 34 and (a) = 45"<<endl;
    
    //a = 45;
    //c = '4';
   //cout << " The value of a is : "<< a <<endl;
    cout << "Constants"<<endl;
    // Constants
    // const int a = 3;
    // cout << " The value of a was : "<< a << endl;
    
// Manipulators 
/*Manipulators are helping functions that can modify the input/output stream. It does not mean that we change the value of a variable, it only modifies the I/O stream using insertion (<<) and extraction (>>) operators */

    cout << "Manipulators"<<endl;
    // int a = 3, b = 78, c = 1233;
    // cout<<"Given the value of int a = 3, b = 78, c = 123"<<a<<endl;
    // cout<<"The Value of a is :"<<setw(4)<<a<<endl;
    // cout<<"The Value of b is :"<<setw(4)<<b<<endl;
    // cout<<"The Value of c is :"<<setw(4)<<c<<endl;
    // cout<<"Without setw(4)"<<endl;
    // cout<<"The Value of a is :"<<a<<endl;
    // cout<<"The Value of b is :"<<b<<endl;
    // cout<<"The Value of c is :"<<c<<endl;
      
     cout << "Operator Precedence"<<endl;
     int a = 1, b = 4;
     //int c = (a*5)+b;
     int c = ((((a*5)+b)-45)+89);
     
     cout<<c;
    

    return 0;
}
