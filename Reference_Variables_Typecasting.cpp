// C++ Reference Variables & Typecasting

#include <iostream>;
using namespace std;

int d = 45;  // add a global variable

int main(){
    // Build in Data Types
    //int a, b, c, d;
    // cout << "Enter The Value of A :"<< endl;
    // cin >> a;
    // cout << "Enter The Value of B :"<< endl;
    // cin >> b;
    // cout << "Enter The Value of C :"<< endl;
    // cin >> c;
    // d = a+b+c;
    // cout << "The SUM of A ,B and C is : "<< d << endl; // local variable
    // cout << "The Global Variable of A ,B and C is : "<< ::d; // global variable but output comes with local scope
    // :: scope resolution operator
    // cout << "The Global Variable of A ,B and C is : "<< ::d;
    
    // Float , Double and Long Double Literal
    // float e = 34.4F;
    // long double f = 34.4L;
    // cout << "The value of e is : " << e <<endl;
    // cout << "The value of F is : " << f <<endl;
    // cout << "The value of e = 34.4f is : " << sizeof(34.4f)<<endl;
    // cout << "The value of e = 34.4F is : " << sizeof(34.4F)<<endl; 
    // cout << "The value of e = 34.4l is : " << sizeof(34.4l)<<endl; 
    // cout << "The value of e = 34.4L is : " << sizeof(34.4L)<<endl; 
    
    // Reference Variable in C++
    // Debashish --> Deba, Bacha, Bachadhan, Dhan etc...
    
    
    // float x = 455;
    // float & y = x; 
    // cout << x << endl;
    // cout << y;
    
    // Typecasting in C++
    int a = 45;
    float b = 45.6;
    cout << " Given the value of int a = 45,float b = 45.6 and int c = int(b) "<<endl;
    // cout <<"The Value of a is : " << (float)a<< endl;
    // cout <<"The Value of a is : " << float(a)<< endl;
    
    // cout <<"The Value of b is : " << (int)b<<endl;
    // cout <<"The Value of b is : " << int(b)<<endl;
    
    int c = int(b);
    cout << "The expresion is "<< a + b<< endl;
    cout << "The expresion is "<< a + int(b)<< endl;
    cout << "The expresion is "<< a + (int)b<< endl;
    
    return 0;
}
